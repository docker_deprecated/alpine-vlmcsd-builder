ARG BUILD_ARCH=x64

FROM forumi0721alpinex64build/alpine-x64-buildbase:latest as vlmcsd

LABEL maintainer="forumi0721@gmail.com"

COPY local/. /usr/local/

#RUN ["docker-build-start"]

RUN ["docker-init"]

#RUN ["docker-build-end"]



FROM forumi0721/busybox-${BUILD_ARCH}-base:latest

LABEL maintainer="forumi0721@gmail.com"

COPY --from=vlmcsd /output /output

