# alpine-vlmcsd-builder

#### [alpine-x64-vlmcsd-builder](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-vlmcsd-builder/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinex64build/alpine-x64-vlmcsd-builder/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinex64build/alpine-x64-vlmcsd-builder/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinex64build/alpine-x64-vlmcsd-builder/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64build/alpine-x64-vlmcsd-builder)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64build/alpine-x64-vlmcsd-builder)
#### [alpine-aarch64-vlmcsd-builder](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-vlmcsd-builder/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpineaarch64build/alpine-aarch64-vlmcsd-builder/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpineaarch64build/alpine-aarch64-vlmcsd-builder/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpineaarch64build/alpine-aarch64-vlmcsd-builder/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64build/alpine-aarch64-vlmcsd-builder)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64build/alpine-aarch64-vlmcsd-builder)
#### [alpine-armhf-vlmcsd-builder](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-vlmcsd-builder/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinearmhfbuild/alpine-armhf-vlmcsd-builder/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinearmhfbuild/alpine-armhf-vlmcsd-builder/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinearmhfbuild/alpine-armhf-vlmcsd-builder/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhfbuild/alpine-armhf-vlmcsd-builder)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhfbuild/alpine-armhf-vlmcsd-builder)




----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [vlmcsd](https://forums.mydigitallife.info/threads/50234-Emulated-KMS-Servers-on-non-Windows-platforms) (Build Image)
    - Emulated KMS Servers on non-Windows platforms.



----------------------------------------
#### Run

* Nothing



----------------------------------------
#### Usage

* Nothing



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

